output "aks_vnet_subnet_id" {
  value = "${azurerm_subnet.aks_subnet.*.id}"
}